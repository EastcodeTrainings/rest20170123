CREATE TABLE beer (
  id IDENTITY,
  title VARCHAR(255),
  degrees INTEGER NOT NULL,
  brewery_id INTEGER NOT NULL
);

CREATE TABLE brewery (
  id IDENTITY,
  name VARCHAR(255)
);