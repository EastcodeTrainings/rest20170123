INSERT INTO brewery VALUES (1, 'SAP Hiller');
INSERT INTO brewery VALUES (2, 'Javabeer');
INSERT INTO brewery VALUES (3, 'Drink#');

INSERT INTO beer VALUES (1, 'Hana Ten', 10, 1);
INSERT INTO beer VALUES (2, 'Hana Twelve', 12, 1);
INSERT INTO beer VALUES (3, 'Tiger', 5, 2);
INSERT INTO beer VALUES (4, 'Wild Mustang', 6, 2);
INSERT INTO beer VALUES (5, 'Watery Dolphin', 7, 2);