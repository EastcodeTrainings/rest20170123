package sk.eastcode.beerbar;

public class BeerNotFoundException extends RuntimeException {
    private Long id;

    public BeerNotFoundException(Long id) {
        super("Beer " + id + " was not found");
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}