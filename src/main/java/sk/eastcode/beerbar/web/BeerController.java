package sk.eastcode.beerbar.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import sk.eastcode.beerbar.Beer;
import sk.eastcode.beerbar.BeerNotFoundException;
import sk.eastcode.beerbar.dao.BeerDao;

import java.util.List;

@RestController
public class BeerController {
    @Autowired
    private BeerDao beerDao;

    @RequestMapping("/beers")
    public List<Beer> getAllBeers() {
        return beerDao.findAll();
    }

    @RequestMapping("/beers/{beerId}")
    public Beer getBeer(@PathVariable Long beerId) {
        Beer beer = beerDao.findById(beerId);
        if(beer == null) {
            throw new BeerNotFoundException(beerId);
        }
        return beer;
    }

    @RequestMapping(value = "/beers", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addBeer(@RequestBody Beer beer) {
        beerDao.saveOrUpdate(beer);
    }


}
