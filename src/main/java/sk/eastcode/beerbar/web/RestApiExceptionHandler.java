package sk.eastcode.beerbar.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import sk.eastcode.beerbar.BeerNotFoundException;

@ControllerAdvice
public class RestApiExceptionHandler {

    @ExceptionHandler(BeerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    ApiError handleBeerNotFoundException(BeerNotFoundException e) {
        ApiError apiError = new ApiError();
        apiError.setMessage("Beer " + e.getId());
        apiError.setStatus("404");

        return apiError;
    }
}
