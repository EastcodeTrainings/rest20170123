package sk.eastcode.beerbar.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import sk.eastcode.beerbar.Brewery;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SqlBreweryDao implements BreweryDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RowMapper<Brewery> breweryRowMapper;

    private SimpleJdbcInsert insertBreweryCommand;

    @PostConstruct
    public void initialize() {
        insertBreweryCommand = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("brewery")
                .usingGeneratedKeyColumns("id");
    }

    public List<Brewery> findAll() {
        return jdbcTemplate.query(Queries.Brewery.BasicQuery, breweryRowMapper);
    }

    public Brewery findById(Long id) {
        try {
            return jdbcTemplate.queryForObject(Queries.Brewery.BasicQuery + " WHERE brewery.id = ?", breweryRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public void saveOrUpdate(Brewery brewery) {
        if(brewery.getId() == null) {
            Map<String, Object> row = new LinkedHashMap<String, Object>();
            row.put("name", brewery.getName());

            Number id = insertBreweryCommand.executeAndReturnKey(row);
            brewery.setId(id.longValue());
        } else {
            jdbcTemplate.update("UPDATE brewery SET `name` = ? WHERE id = ?", brewery.getName(), brewery.getId());
        }
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setBreweryRowMapper(RowMapper<Brewery> breweryRowMapper) {
        this.breweryRowMapper = breweryRowMapper;
    }
}
