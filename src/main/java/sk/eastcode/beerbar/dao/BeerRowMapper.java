package sk.eastcode.beerbar.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import sk.eastcode.beerbar.Beer;
import sk.eastcode.beerbar.Brewery;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BeerRowMapper implements RowMapper<Beer> {
    @Autowired
    private RowMapper<Brewery> breweryRowMapper;

    public Beer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Beer beer = new Beer();
        beer.setId(rs.getLong("beer_id"));
        beer.setTitle(rs.getString("beer_title"));
        beer.setDegrees(rs.getInt("beer_degrees"));
        beer.setBrewery(breweryRowMapper.mapRow(rs, rowNum));

        return beer;
    }

    public void setBreweryRowMapper(RowMapper<Brewery> breweryRowMapper) {
        this.breweryRowMapper = breweryRowMapper;
    }
}
