package sk.eastcode.beerbar.dao;

public class Queries {
    public static final String BasicQuery = "SELECT " +
            "beer.id AS beer_id, " +
            "beer.title AS beer_title, " +
            "beer.degrees AS beer_degrees, " +
            "beer.brewery_id, " +
            "brewery.name AS brewery_name " +
            "FROM beer " +
            "JOIN brewery ON brewery.id = brewery_id ";

    public interface Brewery {
        String BasicQuery = "SELECT " +
                "brewery.id AS brewery_id," +
                "brewery.name AS brewery_name " +
                "FROM brewery";
    }
}
