package sk.eastcode.beerbar.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import sk.eastcode.beerbar.Brewery;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BreweryRowMapper implements RowMapper<Brewery> {
    public Brewery mapRow(ResultSet rs, int rowNum) throws SQLException {
        Brewery brewery = new Brewery();
        brewery.setId(rs.getLong("brewery_id"));
        brewery.setName(rs.getString("brewery_name"));

        return brewery;
    }
}
