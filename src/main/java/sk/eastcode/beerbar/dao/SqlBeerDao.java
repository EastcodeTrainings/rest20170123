package sk.eastcode.beerbar.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sk.eastcode.beerbar.Beer;

import java.util.List;

@Repository
@Transactional
public class SqlBeerDao implements BeerDao {
    public final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RowMapper<Beer> beerRowMapper;

    @Transactional(readOnly = true)
    public List<Beer> findAll() {
        return jdbcTemplate.query(Queries.BasicQuery, beerRowMapper);
    }

    @Transactional(readOnly = true)
    public Beer findById(Long id) {
        try {
            return jdbcTemplate.queryForObject(Queries.BasicQuery + "WHERE beer.id = ?", beerRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<Beer> findByBreweryId(Long breweryId) {
        return jdbcTemplate.query(Queries.BasicQuery + "WHERE brewery.id = ?", beerRowMapper, breweryId);
    }

    @Transactional
    public void saveOrUpdate(Beer beer) {
        String sql = "INSERT INTO beer VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql, beer.getId(), beer.getTitle(), beer.getDegrees(), beer.getBrewery().getId());
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setBeerRowMapper(RowMapper<Beer> beerRowMapper) {
        this.beerRowMapper = beerRowMapper;
    }
}
