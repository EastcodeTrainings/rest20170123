package sk.eastcode.beerbar.dao;

import sk.eastcode.beerbar.Brewery;

import java.util.List;

public interface BreweryDao {
    List<Brewery> findAll();

    Brewery findById(Long id);

    void saveOrUpdate(Brewery brewery);

}
