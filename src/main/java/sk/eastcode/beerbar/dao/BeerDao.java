package sk.eastcode.beerbar.dao;

import sk.eastcode.beerbar.Beer;

import java.util.List;

public interface BeerDao {
    List<Beer> findAll();

    Beer findById(Long id);

    List<Beer> findByBreweryId(Long breweryId);

    void saveOrUpdate(Beer beer);

}
