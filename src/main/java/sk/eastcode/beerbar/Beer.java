package sk.eastcode.beerbar;

public class Beer {
    private Long id;

    private String title;

    private int degrees;

    private Brewery brewery;

    public Beer() {
        // empty constructor
    }

    public Beer(Long id, String title, int degrees) {
        this.id = id;
        this.title = title;
        this.degrees = degrees;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDegrees() {
        return degrees;
    }

    public void setDegrees(int degrees) {
        this.degrees = degrees;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public Brewery getBrewery() {
        return brewery;
    }
}
